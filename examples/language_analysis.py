import json
import unittest
import sys
sys.path.append('..')
from graphwork import graphwork as gw

results = open('language_results.txt', 'w')

def load_data():
    with open('data.json') as data_file:
        data = json.load(data_file)
    return data

def populate_graph(graph, data):
    paradigm_property = graph.new_node_property([])
    languages = data['langs']
    for language in languages:
        graph.add_node(language['label'])
        paradigms = []
        for paradigm in language['paradigms']:
            paradigms.append(paradigm['name'])
        paradigm_property[language['label']] = paradigms
    for language in languages:
        langs_influenced = language['influenced']
        for lang_influenced in langs_influenced:
            if graph.has_node(lang_influenced['name']):
                graph.add_edge((language['label'], lang_influenced['name']))
    graph.attach_property(paradigm_property)

def line_seperator():
    results.write("\n-----------------------------------------------------\n")

def influence_analysis(graph, language):
    results.write('Languages influenced by ' + language)
    line_seperator()
    for language in graph.neighbors(language):
        results.write(language + ", ")
    results.write("\n\n")

def influenced_by(graph, languages):
    results.write('Languages influenced by both ' + str(languages))
    line_seperator()
    for language in graph.common_neighbors(languages):
        results.write(language + ", ")
    results.write("\n\n")

def paradigm_analysis(graph, paradigm):
    results.write('Languages following the ' + paradigm + ' paradigm')
    line_seperator()
    paradigms = graph.properties[0]
    for language in graph.nodes().where(lambda x: paradigm in paradigms[x]):
        results.write(language + ", ")
    results.write("\n\n")

def language_and_paradigm(graph, language, paradigm):
    results.write('Languages following the ' + paradigm + ' paradigm influenced by ' + language)
    line_seperator()
    paradigms = graph.properties[0]
    filtered_set = graph\
                    .neighbors(language)\
                    .where(lambda x: paradigm in paradigms[x])
    for language in filtered_set:
        results.write(language + ", ")
    results.write("\n\n")

def main():
    data = load_data()
    graph = gw.DGraph()
    populate_graph(graph, data)
    influence_analysis(graph, 'Python')
    influenced_by(graph, ['Python', 'Ruby'])
    paradigm_analysis(graph, 'Structured programming')
    language_and_paradigm(graph, 'Python', 'Structured programming')


if __name__=="__main__":
    main()
