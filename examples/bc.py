import random
import unittest
import sys
sys.path.append('..')
from graphwork import graphwork as gw

def generate_weighted_directed():
    graph = gw.DGraph()

    # Populate with 20 nodes
    for i in range(8):
        graph.add_node(i)

    num_added = 0
    # Add until graph connected
    while not graph.is_connected():
        next_edge = (random.randint(0, 7), random.randint(0, 7))
        if next_edge[0] == next_edge[1]:
            continue
        if not graph.has_edge(next_edge):
            graph.add_edge(next_edge)
    # Add 50 more random edges
    while num_added < 6:
        next_edge = (random.randint(0, 7), random.randint(0, 7))
        if next_edge[0] == next_edge[1]:
            continue
        if not graph.has_edge(next_edge):
            num_added += 1
            graph.add_edge(next_edge)

    return graph

def bc(graph):
    bc = graph.new_node_property(0)
    for s in graph.nodes():
        sigma = graph.new_node_property(0)
        delta = graph.new_node_property(0)
        sigma[s] = 1
        for v in graph.in_bfs(s).where(lambda x: x != s):
            sigma[v] = sum([sigma[w] for w in graph.parents(v)])
        for v in reversed(graph.in_bfs(s).where(lambda x: x != s)):
            delta[v] = sum([sigma[v] / sigma[w] * (1 + delta[w]) for w in graph.children(v)])
            bc[v] += delta[v]
    return bc

def main():
    graph = generate_weighted_directed()
    print('graph generated')
    res = bc(graph)
    node = graph.nodes().select(max, res)
    print('graph:')
    for node in graph.nodes():
        print('\tnode: {0}'.format(node))
        print('\t\t{0}'.format(graph.children(node)))
    print(str(node) + ': ' + str(res[node]))

if __name__=="__main__":
    main()
