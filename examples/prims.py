import random
import unittest
import sys
sys.path.append('..')
from graphwork import graphwork as gw

results = open('prims_results.txt', 'w')

def line_seperator():
    results.write("\n-----------------------------------------------------\n")

def print_graph(graph):
    for node in graph.nodes():
        results.write(str(node) + ": ")
        for neighbor in graph.neighbors(node):
            results.write("(" + str(node) + ", " + str(neighbor) + ")")
        results.write("\n")
    results.write("\n\n")

def generate_weighted_directed():
    graph = gw.Graph()

    # Populate with 20 nodes
    for i in range(20):
        graph.add_node(i)

    num_added = 0
    weights = graph.new_edge_property(0)
    # Add until graph connected
    while not graph.is_connected():
        next_edge = (random.randint(0, 19), random.randint(0, 19))
        if not graph.has_edge(next_edge):
            graph.add_edge(next_edge)
            weights[next_edge] = random.randint(0, 10)
    # Add 50 more random edges
    while num_added < 50:
        next_edge = (random.randint(0, 19), random.randint(0, 19))
        if not graph.has_edge(next_edge):
            num_added += 1
            graph.add_edge(next_edge)
            weights[next_edge] = random.randint(0, 10)
    graph.attach_property(weights)

    results.write("Graph generated")
    line_seperator()
    print_graph(graph)

    return graph

"""
Input: A non-empty connected weighted graph with vertices V and edges E (the weights can be negative).
Initialize: V_new = {x}, where x is an arbitrary node (starting point) from V, E_new = {}
Repeat until V_new = V:
Choose an edge {u, v} with minimal weight such that u is in V_new and v is not (if there are multiple edges with the same weight, any of them may be picked)
Add v to V_new, and {u, v} to E_new
Output: V_new and E_new describe a minimal spanning tree
"""
def prims(graph):
    mst = gw.Graph(nodes=[graph.first_node()])
    weights = graph.properties[0]
    while mst.size() < graph.size():
        min_weight_edge = graph.edges()\
                        .where(lambda x: (x[0] in mst.nodes() and x[1] not in mst.nodes())\
                                or (x[0] not in mst.nodes() and x[1] in mst.nodes()))\
                        .select(min, weights)
        mst.add_edge(min_weight_edge)
        u, v = min_weight_edge
        if mst.has_node(u):
            mst.add_node(v)
        else:
            mst.add_node(u)

    results.write("Minimum Spanning Tree")
    line_seperator()
    print_graph(mst)

def main():
    graph = generate_weighted_directed()
    prims(graph)


if __name__=="__main__":
    main()
