import unittest
import sys
from graphwork.graphwork import DGraph

class TestSorted(unittest.TestCase):

    def setUp(self):
        self.g = DGraph()
        self.color = self.g.new_node_property('black')
        self.weight = self.g.new_edge_property(0)
        self.g.add_nodes([1, 2, 3, 4, 5])
        self.g.add_edges([(1, 2), (1, 3), (3, 4), (1, 5)])

        self.weight[(1, 2)] = 3
        self.weight[(1, 3)] = 4
        self.weight[(3, 4)] = -2

        self.color[1] = 'blue'
        self.color[2] = 'azure'
        self.color[3] = 'baby blue'
        self.color[4] = 'indigo'

    def test_sorted_nodes(self):
        sorted_nodes = self.g.nodes().sorted_by(self.color)
        print(sorted_nodes)
        self.assertTrue(repr(sorted_nodes) == '[2, 3, 5, 1, 4]')
        self.assertTrue([self.color[x] for x in sorted_nodes] ==
                         ['azure', 'baby blue', 'black', 'blue', 'indigo'])

    def test_sorted_edges(self):
        sorted_edges = self.g.edges().sorted_by(self.weight)
        print(sorted_edges)
        self.assertTrue(repr(sorted_edges) == '[(3, 4), (1, 5), (1, 2), (1, 3)]')
        self.assertTrue([self.weight[e] for e in sorted_edges] == [-2, 0, 3, 4])

if __name__=="__main__":
    unittest.main()
