import unittest
import sys
from graphwork.graphwork import DGraph

class TestBFS(unittest.TestCase):

    def setUp(self):
        self.graph = DGraph()
        self.graph.add_node(1)
        self.graph.add_node(2)
        self.graph.add_node(3)
        self.graph.add_node(4)
        self.graph.add_node(5)

        self.graph.add_edge((1, 2))
        self.graph.add_edge((1, 3))
        self.graph.add_edge((1, 5))
        self.graph.add_edge((2, 4))
        self.graph.add_edge((3, 2))

    def test_basic_bfs(self):
        nodes = []
        for node in self.graph.in_bfs(1):
            nodes.append(node)
        self.assertEqual([1, 2, 3, 5, 4], nodes)

if __name__=="__main__":
    unittest.main()
