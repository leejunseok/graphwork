import unittest
import sys
from graphwork.graphwork import DGraph

class TestCommonNeighbors(unittest.TestCase):

    def setUp(self):
        self.graph = DGraph()
        self.graph.add_node(1)
        self.graph.add_node(2)
        self.graph.add_node(3)
        self.graph.add_node(4)
        self.graph.add_node(5)

        self.graph.add_edge((1, 2))
        self.graph.add_edge((5, 2))
        self.graph.add_edge((1, 3))
        self.graph.add_edge((5, 1))
        self.graph.add_edge((4, 3))
        self.graph.add_edge((4, 2))

    def test_common_neighbor(self):
        self.assertEqual([2], self.graph.common_neighbors([1, 5]))
        self.assertEqual([2, 3], self.graph.common_neighbors([1, 4]))

    def test_single_input(self):
        self.assertEqual([1, 2], self.graph.common_neighbors([5]))

if __name__=="__main__":
    unittest.main()
