import unittest
import sys
from graphwork.graphwork import DGraph

class TestWhereFunction(unittest.TestCase):

    def setUp(self):
        self.graph = DGraph()
        self.graph.add_node(1)
        self.graph.add_node(2)
        self.graph.add_node(3)

        self.graph.add_edge((1, 2))
        self.graph.add_edge((1, 3))
        self.graph.add_edge((2, 3))

        self.color = self.graph.new_node_property("black")
        self.color[1] = "red"
        self.color[2] = "red"
        self.color[3] = "green"

        self.shape = self.graph.new_node_property("polygon")
        self.shape[1] = "circle"
        self.shape[2] = "square"
        self.shape[3] = "oval"

        self.weight = self.graph.new_edge_property(0)
        self.weight[(1, 2)] = 5
        self.weight[(1, 3)] = 1
        self.weight[(2, 3)] = 4

    def test_colors_set(self):
        self.assertTrue(1 in self.graph.nodes())
        self.assertEqual(self.color[1], "red")
        self.assertEqual(self.color[2], "red")
        self.assertEqual(self.color[3], "green")

    def test_where_red(self):
        reds = self.graph.nodes().where(lambda x: self.color[x] == "red")
        self.assertEqual(2, len(reds))
        self.assertTrue(1 in reds and 2 in reds)

    def test_where_red_and_circle(self):
        nodes = self.graph.nodes().where(lambda x: self.color[x] == "red").where(lambda x: self.shape[x] == "circle")
        self.assertEqual(1, len(nodes))
        self.assertTrue(1 in nodes and 2 not in nodes)

    def test_edges_where(self):
        edges = self.graph.edges().where(lambda x: self.weight[x] > 2)
        self.assertEqual(2, len(edges))

if __name__=="__main__":
    unittest.main()
