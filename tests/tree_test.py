import unittest
import sys
from graphwork.graphwork import DGraph

class TestTreeFunctions(unittest.TestCase):

    def setUp(self):
        self.graph = DGraph()
        self.graph.add_node(1)
        self.graph.add_node(2)
        self.graph.add_node(3)

    def test_has_cycles(self):
        self.graph.add_edge((1, 2))
        self.graph.add_edge((2, 3))
        self.graph.add_edge((3, 1))

        self.assertTrue(self.graph.has_cycles())

    def test_has_no_cycles(self):
        self.graph.add_edge((1, 2))
        self.graph.add_edge((1, 3))

        self.assertFalse(self.graph.has_cycles())

if __name__=="__main__":
    unittest.main()
