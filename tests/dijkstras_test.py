import unittest
import sys
from graphwork.graphwork import DGraph

class TestDijkstras(unittest.TestCase):

    def setUp(self):
        self.graph = DGraph()
        self.graph.add_node(1)
        self.graph.add_node(2)
        self.graph.add_node(3)
        self.graph.add_node(4)
        self.graph.add_node(5)

        self.graph.add_edge((1, 2))
        self.graph.add_edge((1, 3))
        self.graph.add_edge((2, 4))
        self.graph.add_edge((3, 5))
        
        self.weights = self.graph.new_edge_property()
        self.weights[(1,2)] = 2
        self.weights[(1,3)] = 1
        self.weights[(2,4)] = 10
        self.weights[(3,5)] = 5
    
    def test_dijkstras(self):
        nodes = self.graph.in_dijkstras(1, self.weights)
        self.assertEqual("[1, 3, 2, 5, 4]", str(nodes))

if __name__=="__main__":
    unittest.main()
