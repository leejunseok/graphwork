import unittest
import sys
from graphwork.graphwork import DGraph

class TestBFS(unittest.TestCase):

    def setUp(self):
        self.graph = DGraph()
        self.graph.add_node(1)
        self.graph.add_node(2)
        self.graph.add_node(3)
        self.graph.add_node(4)
        self.graph.add_node(5)
        self.graph.add_node(6)
        self.graph.add_node(7)

        self.graph.add_edge((1, 2))
        self.graph.add_edge((1, 6))
        self.graph.add_edge((1, 5))
        self.graph.add_edge((5, 7))
        self.graph.add_edge((2, 3))
        self.graph.add_edge((3, 4))

    def test_basic_dfs(self):
        nodes = []
        for node in self.graph.in_dfs(1):
            nodes.append(node)
        self.assertEqual([1, 6, 5, 7, 2, 3, 4], nodes)

if __name__=="__main__":
    unittest.main()
