import unittest
import sys
from graphwork.graphwork import DGraph

class TestBasicGraph(unittest.TestCase):

    def setUp(self):
        self.graph = DGraph()

    def test_add_node(self):
        self.graph.add_node(1)
        nodes = self.graph.nodes()
        self.assertEqual(len(nodes), 1)
        self.assertTrue(1 in nodes)

    def test_add_edge(self):
        self.graph.add_node(1)
        self.graph.add_node(2)
        self.graph.add_edge((1, 2))
        self.assertTrue((1, 2) in self.graph.edges())

if __name__=="__main__":
    unittest.main()
