import unittest
import sys
from graphwork.graphwork import DGraph

class TestSelect(unittest.TestCase):

    def setUp(self):
        self.g = DGraph()
        self.number = self.g.new_node_property(0)
        self.length = self.g.new_edge_property(0)
        self.g.add_nodes([1,2,3,4,5])
        self.g.add_edges([(1,2), (1,3), (2,4), (3,5)])

        self.number[1] = 0
        self.number[2] = 1
        self.number[3] = 2
        self.number[4] = 3
        self.number[5] = 4

        self.length[(1,2)] = 1
        self.length[(1,3)] = 2
        self.length[(2,4)] = 3
        self.length[(3,5)] = 4

    def test_select_nodes(self):
        node = self.g.nodes().select(max, self.number)
        print(node)
        self.assertTrue(repr(node) == '5')
        self.assertTrue(self.number[node] == 4)

    def test_select_edges(self):
        edge = self.g.edges().select(max, self.length)
        print(edge)
        self.assertTrue(repr(edge) == '(3, 5)')
        self.assertTrue(self.length[edge] == 4)

if __name__ == "__main__":
	unittest.main()
