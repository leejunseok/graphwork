import unittest
import sys
from graphwork.graphwork import DGraph

class TestDegreeFunctions(unittest.TestCase):

    def setUp(self):
        self.graph = DGraph()
        self.graph.add_node(1)
        self.graph.add_node(2)
        self.graph.add_node(3)
        self.graph.add_node(4)
        self.graph.add_node(5)

        self.graph.add_edge((1, 2))
        self.graph.add_edge((1, 3))
        self.graph.add_edge((1, 4))
        self.graph.add_edge((5, 2))

    def test_out_degree(self):
        self.assertEqual(3, self.graph.out_degree(1))

    def test_in_degree(self):
        self.assertEqual(2, self.graph.in_degree(2))

if __name__=="__main__":
    unittest.main()
