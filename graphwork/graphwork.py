import networkx as nx
from queue import Queue, PriorityQueue

"""
 Design notes:
 * Edges/Nodes objects don't update with the graph
 * Property get errors if elem not present, but gives default if value not defined
 * Property put errors if elem not present
    this means property has to know when nodes/edges are deleted or added
    could just give each property a 'parent graph' reference, look up every time
"""

class Nodes:
    def __init__(self, nodes=[]):
        self._nodes = nodes
    def __repr__(self):
        return repr(self._nodes)
    def __iter__(self):
        for node in self._nodes:
            yield node
    def __len__(self):
        return len(self._nodes)
    def __getitem__(self, key):
        return self._nodes[key]
    def where(self, pred):
        return Nodes([node for node in self._nodes if pred(node)])
    def sorted_by(self, prop):
        return Nodes(sorted(self._nodes, key=lambda x: prop[x]))
    def select(self, comp, prop):
        length = len(self._nodes)
        if length == 0:
            return None
        if length == 1:
            return self._nodes[0]
        best = self._nodes[0]
        pbest = prop[best]
        for node in self._nodes[1:]:
            p2 = prop[node]
            answer = comp(pbest, p2)
            if answer == p2:
                best = node
                pbest = prop[best]
        return best

class Property:
    def __init__(self, parent, default):
        self._parent = parent
        self._table = {}
        self._default = default

class NodeProperty(Property):
    def __getitem__(self, node):
        if node in self._parent.nodes():
            return self._table.get(node, self._default)
        else:
            raise KeyError("Referenced node does not exist.")
    def __setitem__(self, node, value):
        if node in self._parent.nodes():
            self._table[node] = value
        else:
            raise KeyError("Referenced node does not exist.")
    def __len__(self):
        return len(self._table.keys())
    # By value
    def __contains__(self, value):
        return value in self._table.values()

class Edges:
    def __init__(self, edges=[]):
        self._edges = edges
    def __repr__(self):
        return repr(self._edges)
    def __iter__(self):
        for edge in self._edges:
            yield edge
    def __len__(self):
        return len(self._edges)
    def where(self, pred):
        return Edges([edge for edge in self._edges if pred(edge)])
    def sorted_by(self, prop):
        return Edges(sorted(self._edges, key=lambda x: prop[x]))
    def select(self, comp, prop):
        length = len(self._edges)
        if length == 0:
            return None
        if length == 1:
            return self._edges[0]
        best = self._edges[0]
        pbest = prop[best]
        for edge in self._edges[1:]:
            p2 = prop[edge]
            answer = comp(pbest, p2)
            if answer == p2:
                best = edge
                pbest = prop[best]
        return best


class DEdgeProperty(Property):
    def __getitem__(self, edge):
        if edge in self._parent.edges():
            return self._table.get(edge, self._default)
        else:
            raise KeyError("Referenced edge does not exist.")
    def __setitem__(self, edge, value):
        if edge in self._parent.edges():
            self._table[edge] = value
        else:
            raise KeyError("Referenced edge does not exist.")

class EdgeProperty(Property):
    def __getitem__(self, edge):
        u, v = edge
        if ((u, v) in self._parent.edges() or
                (v, u) in self._parent.edges()):
            if (u, v) in self._table:
                return self._table[(u,v)]
            else:
                return self._table.get((v, u), self._default)
        else:
            raise KeyError("Referenced edge does not exist.")
    def __setitem__(self, edge, value):
        u, v = edge
        if ((u, v) in self._parent.edges() or
                (v, u) in self._parent.edges()):
            if (u, v) in self._table:
                self._table[(u,v)] = value
            else:
                self._table[(v,u)] = value
        else:
            raise KeyError("Referenced edge does not exist.")

class Graph:
    def __init__(self, nodes=[], edges=[]):
        self._graph = nx.Graph()
        self.add_nodes(nodes)
        self.add_edges(edges)
        self.properties = []

    def attach_property(self, prop):
        self.properties.append(prop)

    def new_node_property(self, default=None):
        return NodeProperty(self, default)
    def new_edge_property(self, default=None):
        return EdgeProperty(self, default)

    def add_node(self, node):
        self._graph.add_node(node)
    def rm_node(self, node):
        self._graph.remove_node(node)
    def add_nodes(self, nodes):
        self._graph.add_nodes_from(nodes)
    def rm_nodes(self, nodes):
        self._graph.remove_nodes_from(nodes)

    def first_node(self):
        return self._graph.nodes()[0]

    def has_node(self, node):
        return node in self._graph.nodes()
    def has_edge(self, edge):
        return edge in self._graph.edges()
    def size(self):
        return len(self._graph.nodes())

    def add_edge(self, edge):
        u, v = edge
        self._graph.add_edge(u, v)
    def rm_edge(self, edge):
        self._graph.remove_edge(edge)
    def add_edges(self, edges):
        self._graph.add_edges_from(edges)
    def rm_edges(self, edges):
        self._graph.remove_edges_from(edges)

    def nodes(self):
        return Nodes(self._graph.nodes())
    def edges(self):
        return Edges(self._graph.edges())
    def neighbors(self, node):
        return Nodes(self._graph.neighbors(node))
    def degree(self, node):
        return self._graph.degree(node)
    def successors(self, node):
        return self.neighbors(node)

    def common_neighbors(self, nodes):
        if len(nodes) == 0:
            return []
        if len(nodes) == 1:
            return self.neighbors(nodes[0])
        commons = self._graph.neighbors(nodes[0])
        for node in nodes[1:]:
            current_neighbors = self._graph.neighbors(node)
            commons = [x for x in commons if x in current_neighbors]
            if len(commons) == 0:
                return []
        return commons

    def in_bfs(self, start):
        fringe = Queue()
        result = []
        visited = self.new_node_property(False)
        fringe.put(start)
        while not fringe.empty():
            node = fringe.get()
            if not visited[node]:
                visited[node] = True
                result.append(node)
                for neighbor in self.successors(node):
                    fringe.put(neighbor)
        return Nodes(result)

    def in_dfs(self, start):
        fringe = []
        result = []
        visited = self.new_node_property(False)
        fringe.append(start)
        while len(fringe) > 0:
            node = fringe.pop()
            if not visited[node]:
                visited[node] = True
                result.append(node)
                for neighbor in self.successors(node):
                    fringe.append(neighbor)
        return Nodes(result)

    def in_dijkstras(self, start, weight):
        fringe = PriorityQueue()
        result = []
        distance = self.new_node_property(float('inf'))
        visited = self.new_node_property(False)
        distance[start] = 0
        fringe.put((distance[start], start))
        while not fringe.empty():
            dist, node = fringe.get()
            if not visited[node]:
                visited[node] = True
                distance[node] = dist
                result.append(node)
                for neighbor in self.successors(node):
                    fringe.put((distance[node] + weight[(node, neighbor)], neighbor))
        return Nodes(result)

    def is_connected(self):
        return nx.is_connected(self._graph)

    def has_cycles(self):
        fringe = []
        visited = self.new_node_property(False)
        fringe.append(self._graph.nodes()[0])
        while len(fringe) > 0:
            node = fringe.pop()
            if not visited[node]:
                visited[node] = True
                for neighbor in self.successors(node):
                    fringe.append(neighbor)
            else:
                return True
        return False

    def is_tree(self):
        return self.is_connected() and not self.has_cycles()

class DGraph(Graph):
    def __init__(self, nodes=[], edges=[]):
        self._graph = nx.DiGraph()
        self.add_nodes(nodes)
        self.add_edges(edges)
        self.properties = []

    def new_edge_property(self, default=None):
        return DEdgeProperty(self, default)

    def successors(self, node):
        return self.children(node)

    def parents(self, node):
        return self._graph.predecessors(node)
    def children(self, node):
        return self._graph.successors(node)
    def out_degree(self, node):
        return self._graph.out_degree(node)
    def in_degree(self, node):
        return self._graph.in_degree(node)

    def is_connected(self):
        for node in self.nodes():
            visited = self.new_node_property(False)
            for n in self.in_dfs(node):
                visited[n] = True
            if len(self.nodes().where(lambda x: visited[x])) != len(self.nodes()):
                return False
        return True

    def has_cycles(self):
        ancestor = self.new_node_property(False)
        visited = self.new_node_property(False)
        def visit(n):
            if ancestor[n]:
                return True
            elif visited[n]:
                return False
            ancestor[n] = True
            if any(map(visit, self.successors(n))):
                return True
            ancestor[n] = False
            visited[n] = True
        return any(map(visit, self.nodes()))

    def is_tree(self):
        g = Graph(self.nodes(), self.edges())
        return g.is_tree()

